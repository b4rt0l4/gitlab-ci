# GitLab registry
login:
	docker login registry.gitlab.com


# GitLab registry
# Build
build_python_alpine_3_11_5_registry_image:
	cd python/alpine/3.11.5 && $(MAKE) build_registry_image

build_python_alpine_3_11_6_registry_image:
	cd python/alpine/3.11.6 && $(MAKE) build_registry_image

build_python_alpine_3_12_0_registry_image:
	cd python/alpine/3.12.0 && $(MAKE) build_registry_image

build_python_alpine_3_12_5_registry_image:
	cd python/alpine/3.12.5 && $(MAKE) build_registry_image

build_python_slim_bookworm_3_12_3_registry_image:
	cd python/slim-bookworm/3.12.3 && $(MAKE) build_registry_image

build_registry_image:
	$(MAKE) build_python_alpine_3_11_5_registry_image
	$(MAKE) build_python_alpine_3_11_6_registry_image
	$(MAKE) build_python_alpine_3_12_0_registry_image
	$(MAKE) build_python_alpine_3_12_5_registry_image
	$(MAKE) build_python_slim_bookworm_3_12_3_registry_image


# Publish
publish_python_alpine_3_11_5_registry_image:
	cd python/alpine/3.11.5 && $(MAKE) publish_registry_image

publish_python_alpine_3_11_6_registry_image:
	cd python/alpine/3.11.6 && $(MAKE) publish_registry_image

publish_python_alpine_3_12_0_registry_image:
	cd python/alpine/3.12.0 && $(MAKE) publish_registry_image

publish_python_alpine_3_12_5_registry_image:
	cd python/alpine/3.12.5 && $(MAKE) publish_registry_image

publish_python_slim_bookworm_3_12_3_registry_image:
	cd python/slim-bookworm/3.12.3 && $(MAKE) publish_registry_image

publish_registry_image:
	$(MAKE) publish_python_alpine_3_11_5_registry_image
	$(MAKE) publish_python_alpine_3_11_6_registry_image
	$(MAKE) publish_python_alpine_3_12_0_registry_image
	$(MAKE) publish_python_alpine_3_12_5_registry_image
	$(MAKE) publish_python_slim_bookworm_3_12_3_registry_image


# Build & Publish
build_and_publish_alpine_3_11_5_registry_image:
	cd python/alpine/3.11.5 && $(MAKE) build_and_publish

build_and_publish_alpine_3_11_6_registry_image:
	cd python/alpine/3.11.6 && $(MAKE) build_and_publish

build_and_publish_alpine_3_12_0_registry_image:
	cd python/alpine/3.12.0 && $(MAKE) build_and_publish

build_and_publish_alpine_3_12_5_registry_image:
	cd python/alpine/3.12.5 && $(MAKE) build_and_publish

build_and_publish_slim_bookworm_3_12_3_registry_image:
	cd python/slim-bookworm/3.12.3 && $(MAKE) build_and_publish

build_and_publish:
	$(MAKE) build_and_publish_alpine_3_11_5_registry_image
	$(MAKE) build_and_publish_alpine_3_11_6_registry_image
	$(MAKE) build_and_publish_alpine_3_12_0_registry_image
	$(MAKE) build_and_publish_alpine_3_12_5_registry_image
	$(MAKE) build_and_publish_slim_bookworm_3_12_3_registry_image